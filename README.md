# nette-utils

https://packagist.org/packages/nette/utils

## Unofficial documentation
* [*Hidden Gems of PHP Packages: Nette\Utils*
  ](https://www.tomasvotruba.cz/blog/2018/07/30/hidden-gems-of-php-packages-nette-utils/)
  2018-07 Tomas Votruba